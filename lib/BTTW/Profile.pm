package BTTW::Profile;
our $VERSION = '0.009';
use base 'Exporter';

use Params::Profile;
use Moose::Util::TypeConstraints qw[find_type_constraint];

use BTTW::Exception qw[throw try catch];

use Scalar::Util qw/blessed/;

our @EXPORT = qw[define_profile assert_profile];
our @EXPORT_OK = qw[build_profile];

=head1 NAME

BTTW::Profile - L<Params::Profile> helper.

=head1 DESCRIPTION

This module adds some helper functions that wrap the default
L<Params::Profile> behavior and API.

=head1 METHODS

=head2 define_profile

This function is nothing but some syntactic sugar for registering a
L<Params::Profile> profile.

    use BTTW::Profile;

    define_profile method_name => (
        required => [qw/required parameters/]],
        optional => [qw/optional params/]],
        constraint_methods => {
            required => qr[true|false]
        }
    );

Or, using the extended C<typed> profile syntax:

    define_profile method_name => (
        required => [qw/object string/],
        typed => {
            object => 'Package::Name', # Works, isa is called first
            string => 'Str'            # Also works, uses Moose to validate
        }
    );

Alternatively, you can combine the C<required> and C<optional> lists with the
C<typed> specification for a bit better terseness:

    define_profile method_name => (
        required => {
            id => 'Int',
            name => 'Str'
        },
        optional => {
            version => 'Int'
        }
    );

=head3 Wrapped profiles

C<define_profile> is capable of wrapping a L<Moose::Meta::Method> instance
with automatic profile validity assertions. This is implemented by a C<around>
method modifier on the method named by the profile name. The method is only
executed when the profile is valid, and receives the valid parameters as a
HashRef as the first argument.

    define_profile my_method => (
        wrap => 1,
        required => {
            foo => 'Str'
        }
    );

    sub my_method {
        my ($self, $opts, @rest) = @_;

        my $foo = $opts->{ foo };

        ...
    }

    # Elsewhere in Gotham...

    # This works, scalar(@rest) == 0
    $object->my_method(foo => 'bar');

    # Also works, scalar(@rest) == 0
    $object->my_method({ foo => 'bar' });

    # scalar(@rest) == 1
    $object->my_method({ foo => 'bar' }, 'baz');

    # $opts->{ baz } is *not* passed along here, unkown keys are discarded
    $object->my_method({ foo => 'bar', baz => 'zab' });

=head3 Custom wrapped profiles

Sometimes it's useful to have some more control on what data gets profiled
(as often is the case when writing L<Catalyst> L<actions|Catalyst::Action>).
A custom profile wrapper gives full control over what argument is used as the
data source.

    define_profile my_action => (
        wrap => sub { $_[0]->req->params, @_ },
        required => {
            object_id => UUID
        }
    );

    sub my_action : Local {
        my ($self, $opts, $c) = @_;

        ...
    }

A custom profile data wrapper sub is expected to return the data to be
validated as the first argument. Additional arguments may follow after the
data HashRef and are passed along to the method invocation.

=cut

sub define_profile ($%) {
    my $profile_name = shift;
    my %profile_spec = @_;

    my $wrap = delete $profile_spec{ wrap };

    my ($calling_class) = caller 0;

    my $profile = try {
       build_profile(%profile_spec);
    } catch {
        throw('profile/build_failure', sprintf(
            'Caught exception while building %s::%s profile: %s',
            $calling_class,
            $profile_name,
            "$_"
        ));
    };

    # Early return for default behavior (register profile)
    unless ($wrap) {
        return Params::Profile->register_profile(
            method => $profile_name,
            caller => $calling_class,
            profile => $profile
        );
    }

    unless ($calling_class->can('meta')) {
        throw('define_profile/wrap/defining_class_cant_meta', sprintf(
            "Unable to wrap method %s, %s has no meta",
            $profile_name,
            $calling_class
        ));
    }

    unless (ref $wrap eq 'CODE') {
        $wrap = sub {
            if (ref $_[0] eq 'HASH') {
                return @_;
            } else {
                return { @_ };
            }
        };
    }

    $calling_class->meta->add_around_method_modifier($profile_name => sub {
        my $orig = shift;
        my $self = shift;

        # First argumetn is $self again
        my ($data, @trailing_args) = $wrap->(@_);

        unless (ref $data eq 'HASH') {
            throw('params/profile/data_wrapper/invalid_return_value', sprintf(
                'Expected profile wrapper to return a HashRef, got a "%s"',
                $data
            ));
        }

        my $opts = assert_profile($data, compiled_profile => $profile)->valid;

        return $orig->($self, $opts, @trailing_args);
    });

    return;
}

=head2 build_profile

This function implements the logic to implement C<typed> profiles. It expects
a profile defintion as a hash, and returns a hashref representing a profile
defintion that L<Data::FormValidator> will accept.

=cut

sub build_profile (%) {
    my %profile = @_;

    for my $key (qw[required optional]) {
        if (exists $profile{ $key } && ref $profile{ $key } eq 'HASH') {
            $profile{ typed } = {
                %{ exists $profile{ typed } ? $profile{ typed } : {} },
                %{ $profile{ $key } }
            };

            $profile{ $key } = [ keys %{ $profile{ $key } } ];
        }
    }

    my $typed = delete $profile{ typed };

    if($typed) {
        inject_typed_constraints(\%profile, $typed);
    }

    return \%profile;
}

=head2 inject_typed_constraints

This is a bit of Moose integration for L<Params::Profile>. This method
produces a hashref suitable for L<Data::FormValidator>'s constraint_methods
key.

It basically boils down to an implicit validator-chain built from combining
the C<typed> key in the validator profile and the C<constraint_methods> in a
subref that checks first if the supplied value L<UNIVERSAL::isa> specific type,
and tries to apply Moose's typesystem as a fallback.

There is one major caveat though. L<Data::FormValidator> constraint_methods
can be more than just a coderef or regexp, this code will unelegantly fail
catastrophically in that case. Deal with it.

=cut

sub inject_typed_constraints {
    my $profile = shift;
    my $types = shift;

    my $constraints = $profile->{ constraint_methods } || {};
    my $filters = $profile->{ field_filters } || {};

    for my $fieldname (keys %{ $types }) {
        my $field_type = $types->{ $fieldname };

        # Attempt lookup of a moose type constraint by name
        my $type = find_type_constraint($field_type) || $field_type;

        if (exists $constraints->{ $fieldname }) {
            throw('params/profile/validation_chain', sprintf(
                'A constraint method for field "%s" is already defined',
                $fieldname
            ));
        }

        my $constraint;
        my $filter;

        # Build a trampoline for the concrete validator.
        # If it's a moose type, attempt coercion
        if (ref($type) eq 'CODE') {
            $constraint = $type;
        } elsif (blessed $type && $type->isa('Moose::Meta::TypeConstraint')) {
            $constraint = sub { return $type->check(pop) };
            $filter = sub { return $type->coerce(shift) } if $type->has_coercion;
        } else {
            $constraint = sub {
                my $value = pop;

                return blessed $value && $value->isa($type);
            }
        }

        $constraints->{ $fieldname } = $constraint if defined $constraint;
        $filters->{ $fieldname } = $filter if defined $filter;
    }

    # Rewrite profile with new constraints and filters
    $profile->{ constraint_methods } = $constraints;
    $profile->{ field_filters } = $filters;

    return;
}

=head2 assert_profile(\%params, $string_method || \%profile )

Return: Exception C<params/profile>

Assert a given parameter profile by validating it and throwing an exception on
failure.

B<Options>

=over 4

=item params [required]

The parameters to test, you will probably use C<< $c->req->params >>.

=item method OR profile [optional]

When given a method name, it will find out the profile for you by using the
caller and finding the profile registered by register_profile.

When the second parameters is a HashRef, it will use it as the profile.

=back

=cut

sub assert_profile {
    my ($data, %params) = @_;

    unless(ref $data eq 'HASH') {
        throw('params/profile/input', 'Input not a HashRef, unable to assert profile');
    }

    my $dv;

    if (exists $params{ compiled_profile }) {
        $dv = Data::FormValidator->check($data, $params{ compiled_profile });
    } elsif (exists $params{ profile }) {
        unless(ref $params{ profile } eq 'HASH') {
            throw('params/profile/config', 'Parameter "profile" expected to be a HASH ref.');
        }

        $dv = Data::FormValidator->check($data, build_profile(
            %{ $params{ profile } }
        ));
    } elsif (exists $params{ method }) {
        $dv = Params::Profile->check(
            params => $data,
            method => "$params{ method }"
        );
    } else {
        $dv = Params::Profile->check(
            params => $data,
            method => (caller 1)[3]
        );
    }

    my $expected_isa = 'Data::FormValidator::Results';
    unless (eval { $dv->isa($expected_isa) }) {
        throw(
            'params/profile/profile_definition_invalid',
            sprintf(
                "This is not a '%s', but a '%s', unable to parse the profile",
                $expected_isa, ref $dv
            )
        );
    }

    return $dv if $dv->success;

    my $message = format_dv($dv);

    if (exists $params{ error_wrapper }) {
        unless(ref $params{ error_wrapper} eq 'CODE') {
            throw('params/profile/config', 'Parameter "error_wrapper" expected to be a CODE ref.');
        }

        $message = $params{ error_wrapper }->($message);
    }

    throw(
        'params/profile',
        $message,
        $dv
    );
}

=head2 format_dv

Formats a L<Data::FormValidator::Results> object as a human readable string.

=cut

sub format_dv {
    my $dv = shift;

    my @message_parts = sprintf("Validation of profile failed for %s::%s, line %d", (caller(1))[0, 3, 2]);

    if ($dv->has_invalid) {
        push @message_parts, sprintf("  invalid: %s", join(', ', $dv->invalid));
    }

    if ($dv->has_missing) {
        push @message_parts, sprintf("  missing: %s", join(', ', $dv->missing));
    }

    return join ",\n", @message_parts;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
