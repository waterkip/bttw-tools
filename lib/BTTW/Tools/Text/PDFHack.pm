package BTTW::Tools::Text::PDFHack;
our $VERSION = '0.009';
use Moose;

use BTTW::Tools;

=head1 NAME

BTTW::Tools::Text::PDFHack - Reimplementation of
L<BTTW::Tools::Text::PDF> using the C<pdftotext> system binary.

=head1 DESCRIPTION

See L<BTTW::Tools::Text::PDF>.

=head1 ATTRIBUTES

=head2 file

This attribute should hold the path to a PDF to be converted.

=cut

has file => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 plaintext

This method implements the interface for L<BTTW::Tools::Text/source>.

=cut

sub plaintext {
    my $self = shift;
    my $file = $self->file;

    local $/;
    open (my $txt, '-|', '/usr/bin/pdftotext', $file, '-') or throw(
        'text/pdfhack',
        "Unable to open '$file', can't convert PDF to plaintext"
    );

    return <$txt>;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
