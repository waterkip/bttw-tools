package BTTW::Tools::Tests;
our $VERSION = '0.009';
use warnings;
use strict;

use autodie;

use feature ();
use Exporter qw(import);
use IO::Socket::INET;
use IO::Socket::SSL qw(inet4);

our @EXPORT_OK = (
    qw(
        test_plain_connect
        test_secure_connect
    ),
);

our %EXPORT_TAGS = (
    all => \@EXPORT_OK
);

=head2 test_plain_connect

Test if we can connect to a socket on the remote system

=cut

sub test_plain_connect {
    my ($host, $port) = @_;

    my $sock = IO::Socket::INET->new(
        PeerPort => $port,
        PeerAddr => $host,
        Timeout  => 10,
        Proto    => 'tcp',
    );

    if (!$sock) {
        return "$!";
    }

    return;
}

=head2 test_secure_connect

Test if we can connect to a secure socket on the remote system

=cut

sub test_secure_connect {
    my ($host, $port, $ca_cert, $client_key, $client_cert) = @_;

    my %socket_opts = (
        PeerPort => $port,
        PeerAddr => $host,
        Timeout  => 10,
        Proto    => 'tcp',

        SSL_hostname        => $host,
        SSL_verify_mode     => SSL_VERIFY_PEER,
        SSL_verifycn_scheme => 'www'
    );

    if ($ca_cert) {
        $socket_opts{ SSL_ca_file } = $ca_cert;
    } else {
        $socket_opts{ SSL_ca_path } = '/etc/ssl/certs';
    }

    $socket_opts{ SSL_key_file } = $client_key
        if (defined $client_key);
    $socket_opts{ SSL_cert_file } = $client_cert
        if (defined $client_cert);

    my $sock = IO::Socket::SSL->new(%socket_opts);

    if (!$sock) {
        my $error = IO::Socket::SSL::errstr();
        return "$error";
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
