FROM perl:latest

WORKDIR /tmp/build

COPY cpanfile .

RUN apt-get update && apt-get install --no-install-recommends -y \
    # Required by File::ArchivableFormat' File::LibMagic dependency
    libmagic-dev \
    # Required for pdftotext
    poppler-utils \
 && rm -rf /var/lib/apt/lists/*

COPY dev-bin .

# Seperate run for this dependency, since tests fail "occasionally"
RUN ./cpanm --notest IPC::System::Simple \
    && ./cpanm --installdeps .

COPY . .
RUN ./cpanm .
