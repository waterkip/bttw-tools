use warnings;
use strict;
use Test::More;

use BTTW::Tools::UA;

{
    my $ua = new_user_agent();
    isa_ok($ua, 'LWP::UserAgent', 'new_user_agent() returns a new LWP::UserAgent');
    is(
        $ua->agent,
        'BTTW::Tools',
        'When $Zaaksysteem::VERSION is not available, the user-agent string is BTTW::Tools'
    );
    is ($ua->timeout, 10, "Default timeout is 10");
    is_deeply($ua->protocols_allowed, ['https'], "Default protocol_allowed: https only");

    is_deeply([sort $ua->ssl_opts], [qw(SSL_ca_path verify_hostname)], "Default SSL options");

    is($ua->ssl_opts('verify_hostname'), '1', "verify_hostname is on by default");
    is($ua->ssl_opts('SSL_ca_path'), '/etc/ssl/certs', "Default SSL CA path is set");
}

{
    local $BTTW::VERSION = 31337;
    my $ua = new_user_agent();

    is(
        $ua->agent,
        'BTTW/31337',
        'When $BTTW::VERSION is available, the user-agent string is BTTW/$VERSION'
    );
}

{
    my $ua = new_user_agent(timeout => 42);
    is($ua->timeout, 42, "Passing a custom timeout works");
}

{
    my $ua = new_user_agent(
        ca_cert              => '/etc/cacert',
        client_cert          => '/etc/cert',
        client_key           => '/etc/key',
        client_key_passwd_cb => sub { return "super secret password" },
        protocols_allowed    => ['ftp'],
    );

    is_deeply(
        [sort $ua->ssl_opts],
        [qw(SSL_ca_file SSL_cert_file SSL_key_file SSL_passwd_cb verify_hostname)],
        "Custom SSL options"
    );

    is($ua->ssl_opts('verify_hostname'), '1', "verify_hostname is on even with custom SSL options");
    is($ua->ssl_opts('SSL_ca_file'), '/etc/cacert', "Custom CA certificate set");
    is($ua->ssl_opts('SSL_cert_file'), '/etc/cert', "Custom client certificate set");
    is($ua->ssl_opts('SSL_key_file'), '/etc/key', "Custom client certificate private key set");
    is($ua->ssl_opts('SSL_passwd_cb')->(), 'super secret password', "Password callback works");
}

done_testing();
